import 'package:flutter/material.dart';
enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.purpleAccent,
        ),
    );

  }
  static ThemeData appThemeDark()
  {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.purpleAccent,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //theme: MyAppTheme.appThemeLight(),
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton.small(
           onPressed: () {
            setState(() {
             currentTheme == APP_THEME.DARK
              ? currentTheme = APP_THEME.LIGHT
              : currentTheme = APP_THEME.DARK;
            });
          },
          child: currentTheme == APP_THEME.DARK

              ? Icon(Icons.sunny)
              : Icon(Icons.nightlight_sharp),
          backgroundColor: Colors.pinkAccent,
      ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message_rounded,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call_outlined,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDirectionslButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPaylButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call) ,
    title: Text("999-999-9999"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),

  );
}
Widget otherPhoneListTile() {
  return ListTile(
    leading: Text("") ,
    title: Text("123-456-7890"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),

  );
}
Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email_rounded) ,
    title: Text("Luffy@gmail.com"),
    subtitle: Text("Email"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),

  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on) ,
    title: Text("999 Sunset ST, Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),

  );
}
AppBar buildAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.pinkAccent,

    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget> [
      IconButton(
          onPressed: () {},
          icon: Icon(Icons.star_border),
          color: Colors.black
      )
    ],
  );
}
Widget buildBodyWidget(){
  return ListView (
    children: <Widget>[
      Column(
        children: <Widget> [
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network(
              "https://i1.wp.com/www.senpai.com.mx/wp-content/uploads/2021/11/One-Piece_-Fanart-de-Luffy-presenta-todos-los-atuendos-que-ha-usado-en-el-anime.jpg?fit=1280%2C720&ssl=1",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget> [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Monkey D. Luffy",
                    style: TextStyle(fontSize:30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),

          Container(
            margin: const EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.pinkAccent,
              ),
            ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          mobilePhoneListTile(),
          Divider(
            color: Colors.black,
          ),
          otherPhoneListTile(),
          Divider(
            color: Colors.black,
          ),
          emailListTile(),
          Divider(
            color: Colors.black,
          ),
          addressListTile() ,
        ],
      ),
    ],
  );
}
Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[

      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionslButton(),
      buildPaylButton(),
    ],
  );
}

